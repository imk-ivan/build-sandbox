const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require ('mini-css-extract-plugin')

module.exports = {
    // mode: "development",
    mode: "production",
    performance: {
        maxEntrypointSize: 400000,
        maxAssetSize: 1000000,
        hints: false,
    },
    devServer: {
        open: true,
        liveReload: true,
    },
    module: {
        rules:[
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader",
            },
            //Loading css
            {
                test: /\.(css)/,
                use: [MiniCssExtractPlugin.loader, 'css-loader']
            },
            //Loading scss/sass
            {
                test: /\.(s[ca]ss)/,
                use: [MiniCssExtractPlugin.loader,'css-loader','sass-loader']
            },
            //Loading fonts
            {
                test: /\.(ttf|otf|eot|woff|woff2)$/,
                use:[
                    {
                        loader: 'file-loader',
                        options: {
                            outputPath: 'fonts',
                            name: '[name].[ext]'
                        }
                    }
                ]
            },
            //Loading images
            {
                test: /\.(png|jpg|gif|ico|jpeg)$/,
                use:[
                    {
                        loader: 'file-loader',
                        options: {
                            outputPath: 'images',
                            name: '[name]-[sha1:hash:7].[ext]'
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Hello World!',
            buildTime: new Date().toString(),
            template: 'public/index.html'
        }),
        new MiniCssExtractPlugin({
            filename: 'main-[hash:8].css'
        }),
    ]
}