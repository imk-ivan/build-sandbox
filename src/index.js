import React from 'react'
import ReactDOM from 'react-dom';
import './main.scss';


const App = () => <h3>This is Webpack React App!!</h3>;


ReactDOM.render(<App/>, document.getElementById('root'));